resource "aws_iam_role" "scheduler" {
  name                = "${var.name}-scheduler"
  managed_policy_arns = [aws_iam_policy.scheduler.arn]
  assume_role_policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "scheduler.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    "SyncTask" = var.name
  }
}

resource "aws_iam_policy" "scheduler" {
  name        = "${var.name}-scheduler"
  description = "Allow EventBridge to call the ECS Task"
  policy      = data.aws_iam_policy_document.scheduler.json

  tags = {
    "SyncTask" = var.name
  }
}
data "aws_iam_policy_document" "scheduler" {
  version = "2012-10-17"
  statement {
    actions = [
      "ecs:RunTask",
    ]
    effect = "Allow"
    resources = [aws_ecs_task_definition.service.arn]
    condition {
      test     = "ArnLike"
      variable = "ecs:cluster"
      values = [
        aws_ecs_cluster.cluster.arn
      ]
    }
  }
  statement {
    actions = [
      "iam:PassRole",
    ]
    effect    = "Allow"
    resources = [aws_iam_role.taskrole.arn]
  }
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "subnets" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

resource "aws_scheduler_schedule" "scheduler" {
  name = var.name

  flexible_time_window {
    mode = "OFF"
  }

  schedule_expression = "cron(${var.schedule})"

  target {
    arn      = aws_ecs_cluster.cluster.arn
    role_arn = aws_iam_role.scheduler.arn

    ecs_parameters {
      task_definition_arn     = aws_ecs_task_definition.service.arn
      enable_ecs_managed_tags = true

      capacity_provider_strategy {
        capacity_provider = "FARGATE"
      }

      network_configuration {
        assign_public_ip = true
        subnets          = toset(data.aws_subnets.subnets.ids)
      }

      tags = {
        "SyncTask" = var.name
      }
    }
  }
}
