resource "aws_ecs_cluster" "cluster" {
  name = var.name
  tags = {
    "SyncTask" = var.name
  }
}

resource "aws_ecs_cluster_capacity_providers" "providers" {
  cluster_name = aws_ecs_cluster.cluster.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

resource "aws_iam_role" "taskrole" {
  name                = var.name
  managed_policy_arns = [aws_iam_policy.taskrole.arn]
  assume_role_policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    "SyncTask" = var.name
  }
}

resource "aws_iam_policy" "taskrole" {
  name        = var.name
  description = "Allows ECS to upload logs for backup jobs"
  policy      = data.aws_iam_policy_document.taskrole.json

  tags = {
    "SyncTask" = var.name
  }
}
data "aws_iam_policy_document" "taskrole" {
  version = "2012-10-17"
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }

  statement {
    actions = [
      "s3:GetObject"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:s3:::${var.source-bucket-name}/*"
    ]
  }
  statement {
    actions = [
      "s3:ListBucket"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:s3:::${var.source-bucket-name}"
    ]
  }
}

resource "aws_ecs_task_definition" "service" {
  family = "${var.name}-sync"

  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256 # 0.25vcpu
  memory                   = 512
  execution_role_arn       = aws_iam_role.taskrole.arn
  task_role_arn            = aws_iam_role.taskrole.arn

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "ARM64"
  }

  container_definitions = jsonencode([
    {
      name         = "sync"
      image        = "registry.gitlab.com/littlewonders/docker-images/s3-bucket-sync-tool"
      essential    = true
      portMappings = []

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = "test-awslogs",
          awslogs-region        = "eu-west-1",
          awslogs-stream-prefix = "ecs"
        }
      },
      environment = [
        {
          name  = "AZURE_STORAGE_ACCOUNT"
          value = azurerm_storage_account.destination.name
        },
        {
          name  = "SOURCE_BUCKET"
          value = var.source-bucket-name
        },
        {
          name  = "DESTINATION_CONTAINER"
          value = azurerm_storage_container.destination.name
        },
        {
          name = "AZURE_FILE"
          value = jsonencode({
            appId    = azuread_application.app.application_id,
            password = azuread_service_principal_password.password.value,
            tenant   = azuread_service_principal.principal.application_tenant_id
          })
        }
      ]
    }
  ])

  tags = {
    "SyncTask" = var.name
  }
}