output "service-principal-id" {
    value = azuread_service_principal.principal.object_id
}
output "service-principal-secret" {
    value = azuread_service_principal_password.password.value
}