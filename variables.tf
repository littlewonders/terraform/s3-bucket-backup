variable "name" {
    type = string
    description = "Namespace for the task. Should be unique in both AWS and Azure"
}

variable "source-bucket-name" {
    type = string
    description = "The name of the S3 bucket to sync from"
}

variable "resource-group" {
    type = string
    description = "ID of the project's resource group to store objects in"
}

variable "azure-location" {
    type = string
    description = "Which Azure location to store the data in. For example UK South"
}

variable "schedule" {
    type = string
    description = "Crontab schedule of when to run, example 30 7 * * ? *"
    default = "30 0 * * ? *"
}