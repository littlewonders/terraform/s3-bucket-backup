resource "random_id" "destination" {
  byte_length = 8
}

resource "azurerm_storage_account" "destination" {
  name                     = "backup${random_id.destination.hex}"
  resource_group_name      = var.resource-group
  location                 = var.azure-location
  account_kind             = "StorageV2"
  account_tier             = "Standard"
  account_replication_type = "LRS"

  lifecycle {
    prevent_destroy = true
  }
}

resource "azurerm_storage_container" "destination" {
  name                  = "backup${random_id.destination.hex}"
  storage_account_name  = azurerm_storage_account.destination.name
  container_access_type = "private"

  lifecycle {
    prevent_destroy = true
  }
}

resource "azuread_application" "app" {
  display_name = "Backup Task [${var.name}]"
  owners       = [data.azuread_client_config.current.object_id]
}

data "azuread_client_config" "current" {}

data "azurerm_subscription" "primary" {
}

resource "azurerm_role_definition" "writer" {
  name        = "Backup Task [${var.name}]"
  scope       = data.azurerm_subscription.primary.id
  description = "Allows write access to backup bucket for task ${var.name}"

  permissions {
    actions     = ["Microsoft.Storage/storageAccounts/blobServices/containers/read"]
    not_actions = []
    data_actions = [
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/write",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/add/action",
      "Microsoft.Storage/storageAccounts/blobServices/containers/blobs/read"
    ]
  }

  assignable_scopes = [
    data.azurerm_subscription.primary.id,
  ]
}

resource "azuread_service_principal" "principal" {
  application_id               = azuread_application.app.application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]
}


resource "azuread_service_principal_password" "password" {
  service_principal_id = azuread_service_principal.principal.object_id
}

resource "azurerm_role_assignment" "assignment" {
  scope                = data.azurerm_subscription.primary.id
  role_definition_name = azurerm_role_definition.writer.name
  principal_id         = azuread_service_principal.principal.object_id
}
